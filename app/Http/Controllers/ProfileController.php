<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    public function profile($username){

    	$user = User::whereUsername($username)->first();
    	// $user = User::where('username',$username)->first();
    	 //$user = User::where('username','=',$username)->first();

    	//dd($user); its use for knowing user type details
    	return view('user.profile',compact('user'));
    }
}
